IBS Chat Server - Protocol
===============

The protocol describe two participants. A client and a server.
After an established network connection between client and server the server acts passive
until the client starts with an connection attempt.

Login Phase
-----------
 After an established network connection the client have to start with a login message.
 The _login_ message starts with the chat version and _login_ as message type.
 The user name of the user who want's to participate in the chat,
 has to follow in the next line and can not be empty.

````
CHAT/1.0 Login
<Username>
````

 The server can accept the connection and reply with an _ok_ message.

````
CHAT/1.0 Ok
````

 If the user name is in use or is rejected by the server, the server have to reply with an _error_ message.
 The error message and first line have to be separated from each other by an empty line.

````
CHAT/1.0 Error

<reason>
````

Chat Phase
----------
After an user has logged in,  the chat phase begins.

The server will send a list of users which participate in the chat.
The list is following the first line and contains all users, which are currently connected, in a comma separated list.

````
CHAT/1.0 Participants
<usernameA>,<usernameD>,<usernameC>,...
````

The server may send the following messages at any time.

* DistrMessage
* Logout
* Join
* Left

The client can send his messages to the server with the intention to distribute this message to all chat members.
The message is separated from the first line by an empty line and can not be empty, but can use more then one line.
The text message must be stripped from all trailing non visible characters, line spaces or tabs.

````
CHAT/1.0 TextMessage

<Text>
````

The user can leave the chat with a \textit{logout} message.

````
CHAT/1.0 Logout
````

The logout message will not be confirmed by the server.

The server can send messages at any time for all connected users. The message contains
the user name who send this message, the time the message was received by the server
and his text separated by an empty line.

````
CHAT/1.0 DistrMessage
<timestamp>
<from-username>

<Text>
````

The server will send a message to all connected users when an another user left or joined the chat.

````
CHAT/1.0 Join
<username>
````

````
CHAT/1.0 Left
<username>
````

The server may confirm every client message with an error message.