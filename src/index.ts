import * as express from "express"
import {ChatController} from "./controllers/ChatController";
import {ChatIO} from "./controllers/ChatIO";

// Load the configuration file
let config = require("../config.json");

let app: express.Application = express();

// Show a short description message if someone call this address without a websocket
app.get("/", (req, res) => {
    res.end(`
    This address and port are used only for a chatserver.\n
    You may connect to this address with a websocket connection.\n
    You have to use a custom chat protocol.
    `)
});

let server = app.listen(config.port, () => {
    let host = server.address().address;
    let port = server.address().port;

    console.log(`Listening at http://${host}:${port}`)
});

let chatIO = new ChatIO(server);
let chatController = new ChatController(chatIO);

chatController.start();
