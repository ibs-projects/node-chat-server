/**
 * All types of protocol messages.
 */
export class ProtocolTypes {
    public static get LOGIN() {
        return "Login"
    }

    public static get OK() {
        return "Ok"
    }

    public static get ERROR() {
        return "Error"
    }

    public static get JOIN() {
        return "Join"
    }

    public static get LEFT() {
        return "Left"
    }

    public static get LOGOUT() {
        return "Logout"
    }

    public static get TEXT_MESSAGE() {
        return "TextMessage"
    }

    public static get DISTR_MESSAGE() {
        return "DistrMessage"
    }

    public static get PARTICIPANTS() {
        return "Participants"
    }
}

/**
 * A protocol message with version and a type which identify the protocol.
 */
export abstract class ProtocolMessage {
    /**
     * Create a protocol message.
     *
     * @param _type The type of the message.
     */
    public constructor(private _type: string) {}

    /**
     * @return The protocol version.
     */
    public get version(): string {
        return "1.0"
    }

    /**
     * @return The type of this message.
     */
    public get type(): string {
        return this._type
    }
}

/**
 * A message which is expected from the client.
 */
export abstract class ClientMessage extends ProtocolMessage {}

/**
 * A message which is expected from the server.
 */
export abstract class ServerMessage extends ProtocolMessage {}

/**
 * A login message.
 */
export class LoginMessage extends ClientMessage {

    /**
     * @param _username The name of the from which want to log in.
     */
    public constructor(private _username: string) {
        super(ProtocolTypes.LOGIN)
    }

    /**
     * @return The name of the from.
     */
    public get username(): string {
        return this._username
    }
}

/**
 * A message containing a text.
 */
export class TextMessage extends ClientMessage {
    /**
     * @param _text The text.
     */
    public constructor(private _text: string) {
        super(ProtocolTypes.TEXT_MESSAGE)
    }

    /**
     * @return The text.
     */
    public get text() {
        return this._text
    }
}

/**
 * A message which logout a from.
 */
export class LogoutMessage extends ClientMessage {
    public constructor() {
        super(ProtocolTypes.LOGOUT)
    }
}

/**
 * A ok message. It is used to confirm a message.
 */
export class OkMessage extends ServerMessage {
    public constructor() {
        super(ProtocolTypes.OK)
    }
}

/**
 * A error message.
 */
export class ErrorMessage extends ServerMessage {
    /**
     * @param _reason The reason for this error. A description of the error.
     */
    public constructor(private _reason: string) {
        super(ProtocolTypes.ERROR)
    }

    /**
     * @return The reason why this error was send.
     */
    public get reason(): string {
        return this._reason
    }
}

/**
 * The message which contains all participants of this chat.
 */
export class ParticipantsMessage extends ServerMessage {
    /**
     * @param _usernames The names of the users participating in the chat.
     */
    public constructor(private _usernames: string[]) {
        super(ProtocolTypes.PARTICIPANTS)
    }

    /**
     * @return The name of the users participating in the chat.
     */
    public get usernames(): string[] {
        return this._usernames
    }
}

/**
 * The message which distribute a received message.
 */
export class DistTextMessage extends ServerMessage {
    /**
     * @param _username The name of the from which send a text message.
     * @param _text The text to distribute.
     * @param _timestamp The timestamp of this message.
     */
    public constructor(private _username: string, private _text: string, private _timestamp) {
        super(ProtocolTypes.DISTR_MESSAGE)
    }

    /**
     * @return The name of the from who wrote this message.
     */
    public get username(): string {
        return this._username
    }

    /**
     * @return The text of this message.
     */
    public get text(): string {
        return this._text
    }

    /**
     * @return The timestamp of this message.
     */
    public get timestamp(): number {
        return this._timestamp
    }
}

/**
 * A message which says which from join the chat.
 */
export class JoinMessage extends ServerMessage {
    /**
     * @param _username The from name which join the chat.
     */
    public constructor(private _username: string) {
        super(ProtocolTypes.JOIN)
    }

    /**
     * @return The name of the from which join the chat.
     */
    public get username(): string {
        return this._username
    }
}

/**
 * A message which says which from left the chat.
 */
export class LeftMessage extends ServerMessage {
    /**
     * @param _username The from name which left the chat.
     */
    public constructor(private _username: string) {
        super(ProtocolTypes.LEFT)
    }

    /**
     * @return The name of the from which left the chat.
     */
    public get username(): string {
        return this._username
    }
}
