import {Parser} from "../../utils/Parser";
import {StringUtils} from "../../utils/StringUtils";
import {
    ProtocolMessage, ProtocolTypes, LoginMessage, LogoutMessage, TextMessage, OkMessage,
    ErrorMessage, JoinMessage, LeftMessage, ParticipantsMessage, DistTextMessage
} from "./chat-protocol";

export class ChatProtocolParser implements Parser<string, ProtocolMessage> {
    private static VERSION = "CHAT/1.0";

    public from(message: string): ProtocolMessage {
        if (!message) {
            throw new Error("Nothing to parse")
        }

        let lines = message.split("\n");

        let firstLine = lines.shift();

        if (!StringUtils.startsWith(firstLine, ChatProtocolParser.VERSION)) {
            throw new Error("Wrong protocol: require CHAT/1.0")
        }

        let messageType = firstLine.split(" ").pop();

        if (!messageType) {
            throw new Error("Malformed message: require message type {Login | Logout | Text}")
        }

        switch (messageType) {
            case ProtocolTypes.LOGIN:
                return ChatProtocolParser.parseLogin(lines);
            case ProtocolTypes.LOGOUT:
                return ChatProtocolParser.parseLogout();
            case ProtocolTypes.TEXT_MESSAGE:
                return ChatProtocolParser.parseText(lines);
            default:
                throw new Error("Unsupported message type: supported types {Login | Logout | Text}")
        }
    }

    private static parseLogin(lines: string[]): LoginMessage {
        if (lines.length !== 1) {
            throw new Error("Malformed login message")
        }

        let username = lines.shift();

        return new LoginMessage(username.trim())
    }

    private static parseLogout(): LogoutMessage {
        return new LogoutMessage()
    }

    private static parseText(lines: string[]): TextMessage {
        if (lines.length < 2) {
            throw new Error("Malformed text message require a text line")
        }

        lines.shift(); // throw empty line away

        let text = lines.join("\n");

        return new TextMessage(text)
    }

    public to(message: ProtocolMessage): string {
        switch (message.type) {
            case ProtocolTypes.OK:
                return ChatProtocolParser.to_Ok(<OkMessage>message);
            case ProtocolTypes.ERROR:
                return ChatProtocolParser.to_Error(<ErrorMessage>message);
            case ProtocolTypes.JOIN:
                return ChatProtocolParser.to_Join(<JoinMessage>message);
            case ProtocolTypes.LEFT:
                return ChatProtocolParser.to_Left(<LeftMessage>message);
            case ProtocolTypes.PARTICIPANTS:
                return ChatProtocolParser.to_Participants(<ParticipantsMessage>message);
            case ProtocolTypes.DISTR_MESSAGE:
                return ChatProtocolParser.to_DistrMessage(<DistTextMessage>message);
            default:
                throw new Error(`Unsupported message type '${message.type}'`)
        }
    }

    private static to_DistrMessage(message: DistTextMessage) {
        return (
            `CHAT/${message.version} ${message.type}\n` +
            `${message.timestamp}\n` +
            `${message.username}\n` +
            "\n" +
            `${message.text}`
        );
    }

    private static to_Participants(message: ParticipantsMessage) {
        return (
            `CHAT/${message.version} ${message.type}\n` +
            `${message.usernames.join(",")}`
        );
    }

    private static to_Left(message: LeftMessage) {
        return (
            `CHAT/${message.version} ${message.type}\n` +
            `${message.username}`
        );
    }

    private static to_Join(message: JoinMessage) {
        return (
            `CHAT/${message.version} ${message.type}\n` +
            `${message.username}`
        );
    }

    private static to_Error(message: ErrorMessage) {
        return (
            `CHAT/${message.version} ${message.type}\n` +
            "\n" +
            `${message.reason}`
        );
    }

    private static to_Ok(message: OkMessage) {
        return `CHAT/${message.version} ${message.type}`;
    }
}
