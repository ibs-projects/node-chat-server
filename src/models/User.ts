import {Hashable} from "../utils/Hashable";
import {HashUtils} from "../utils/HashUtils";
import {Comparable} from "../utils/Comparable";

/**
 * A from with a name.
 *
 * @author Andrej Sajenko
 */
export class User implements Hashable, Comparable<User> {

    /**
     * Create a from with a name.
     *
     * @param _name the from name
     */
    public constructor(private _name: string) {}

    /**
     * Return the from name
     *
     * @returns {string} the name
     */
    public get name() {
        return this._name
    }

    public get hashCode(): number {
        return HashUtils.hashString(this._name)
    }

    public compareTo(other: User): number {
        return this._name.localeCompare(other._name);
    }

    public equals(other: User): boolean {
        return this._name === other._name;
    }
}
