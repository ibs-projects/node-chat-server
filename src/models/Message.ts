import {User} from "./User";

/**
 * A text message from a from at a time.
 *
 * @author Andrej Sajenko
 */
export class Message {
    /**
     * Create a message.
     *
     * @param _from The from which wrote this message.
     * @param _text The text the from wrote.
     * @param _time The timestamp of this message.
     */
    public constructor(private _from: User, private _text: string, private _time: number) {}

    /**
     * @return The from which wrote this message.
     */
    public get from(): User {
        return this._from
    }

    /**
     * @return The text message.
     */
    public get text(): string {
        return this._text
    }

    /**
     * @return The timestamp.
     */
    public get time(): number {
        return this._time
    }
}
