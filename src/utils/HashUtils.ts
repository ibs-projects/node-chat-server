/**
 * Containing hash utility functions.
 *
 * @author Andrej Sajenko
 */
export class HashUtils {

    /**
     * Generate the hash code out if an string.
     *
     * @param value The value to generate the hash code
     *
     * @return The hash code generated out of the given value.
     */
    public static hashString(value: string): number {
        let hash = 0;

        if (value.length === 0) {
            return hash;
        }

        for (let i = 0; i < value.length; i++) {
            let character = value.charCodeAt(i);
            hash = ((hash << 5) - hash) + character;
            hash = hash & hash; // Convert to 32bit integer
        }

        return hash;
    }
}
