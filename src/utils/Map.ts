import {Comparable} from "./Comparable";

/**
 * A standard observable map (dictionary).
 *
 * @author Andrej Sajenko
 */
export class Map<K extends Comparable<K>, V> {

    private _entries: Entry<K, V>[] = [];
    
    private _onAddListeners: ((key: K, value: V) => void)[] = [];
    private _onRemoveListeners: ((key: K, value: V) => void)[] = [];

    /**
     * Create an empty map.
     */
    public constructor() {}

    /**
     * Put a new entry in the map. If the map contains a value with this key,
     * the value will be overwritten by the new value.
     *
     * @listener All registered add listeners will be called with the new new value and key
     *
     * @param key
     * @param value
     */
    public put(key: K, value: V): void {
        for (let i = 0; i < this._entries.length; i++) {
            let entry = this._entries[i];

            if (entry.key.equals(key)) {
                this._entries[i] = new Entry(key, value);
                return;
            }
        }

        this._entries.push(new Entry(key, value));

        this._onAddListeners.forEach((cb) => cb(key, value))
    }

    /**
     * Get the value which is referenced by this key
     *
     * @param key
     * @returns {any}
     */
    public get(key: K): V {
        for (let i = 0; i < this._entries.length; i++) {
            let entry = this._entries[i];

            if (entry.key.equals(key)) {
                return this._entries[i].value;
            }
        }

        return undefined;
    }

    /**
     * Remove the value which is referenced by this key.
     *
     * @listener All registered remove listeners will be called with the removed key and value
     *
     * @param key
     */
    public remove(key: K): void {
        for (let i = 0; i < this._entries.length; i++) {
            let entry = this._entries[i];

            if (entry.key.equals(key)) {
                let value = this._entries[i].value;
                this._entries.splice(i, 1);
                this._onRemoveListeners.forEach((cb) => cb(key, value));
                return;
            }
        }
    }

    /**
     * @returns {K[]} All keys used in this map.
     */
    public get keySet(): K[] {
        return this._entries.map((e) => e.key)
    }

    /**
     * @returns {V[]} All values this map contains.
     */
    public get values(): V[] {
        return this._entries.map((e) => e.value)
    }

    /**
     * Register an listener for the remove event.
     * Will be called when an element is removed from the map
     *
     * @param callback
     */
    public onRemove(callback: (key: K, value: V) => void) {
        this._onRemoveListeners.push(callback)
    }

    /**
     * Register an listener for the add event.
     * Will ve called when an element is added to the map
     *
     * @param callback
     */
    public onAdd(callback: (key: K, value: V) => void) {
        this._onAddListeners.push(callback)
    }
}

class Entry<K extends Comparable<K>, V> {
    public constructor(private _key: K, private _value: V) {}

    public get key(): K {
        return this._key;
    }

    public get value(): V {
        return this._value;
    }
}
