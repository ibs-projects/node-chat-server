import {Observable} from "./Observable";

/**
 * Collection of elements.
 *
 * @author Andrej Sajenko
 */
export interface Collection<T> extends Observable<T> {

    /**
     * The number of elements this collection contains.
     *
     * @readOnly
     */
    count: number

    /**
     * @return A standard javascript array of this collection
     *
     * @readOnly
     */
    toArray: T[]

    /**
     * Check the collection for an item.
     *
     * @param predicate The function which checks the collection for an item.
     *
     * @return true if the predicate matched an item.
     */
    contains(predicate: (e: T) => boolean): boolean

    /**
     * Traverse this collection with an function.
     *
     * @param f the function wich will be applied on every item of this collection.
     */
    forEach(f: (e: T) => void): void

    /**
     * Filter this collection with a predicate and return a collection of items which passed the filter.
     *
     * @param predicate The function which filter the items.
     *
     * @return A new Collection containing all items the predicate function accepted (returned true).
     */
    filter(predicate: (e: T) => boolean): Collection<T>

    /**
     * Transform every item of this collection and return the new items on a new collection.
     *
     * @param transform The transformation function which return the new item.
     *
     * @return A new Collection containing all transformed items.
     */
    map<E>(transform: (e: T) => E): Collection<E>

    /**
     * Reduce all elements of this collection into one, starting with an init value.
     *
     * Note: The return value has to be of the same type as the result value, but does
     * not have to be of the same type as the collection.
     *
     * @param merge The function which merge two objects into one.
     *
     * @param init The starting item.
     *
     * @return The value has been reduced to. Will be init if the collection is empty.
     */
    reduce<E>(merge: (e1: E, e2: T) => E, init: E): E
}
