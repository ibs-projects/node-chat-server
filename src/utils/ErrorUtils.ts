/**
 * Contains util function to handle errors.
 *
 * @author Andrej Sajenko
 */
export class ErrorUtils {

    /**
     * Dump an error into the
     *
     * @param err The Error to dump to the error log.
     */
    public static dump(err: Error): void {
        // For runtime check.
        // This function must not trigger an another error at runtime.
        // So checks are required!
        if (typeof err === 'object') {
            if (err.message) {
                console.error('\nMessage: ' + err.message)
            }
            if (err.stack) {
                console.error('\nStacktrace:');
                console.error('====================');
                console.error(err.stack);
            }
        } else {
            console.error('dumpError :: argument is not an object');
        }
    }
}
