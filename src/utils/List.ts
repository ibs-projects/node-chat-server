import {Collection} from "./Collection";

/**
 * An observable list of objects.
 *
 * @author Andrej Sajenko
 */
export class List<T> implements Collection<T> {
    private _items: T[];
    private _addListener: ((item: T) => void)[];
    private _removeListener: ((newItem: T) => void)[];
    private _updateListener: ((oldItem: T, newItem: T) => void)[];

    /**
     * Create a empty list or an list of elements provided by the argument.
     *
     * @param items (Optional) A array of items which will be taken into the List.
     */
    public constructor(private items: T[] = []) {
        this._items = items;
        this._addListener = [];
        this._removeListener = [];
        this._updateListener = [];
    }

    /**
     * Get an element by his index.
     * 
     * @param index The index. Should be < this.count
     * 
     * @return The element at this index or undefined if index out of bound.
     */
    public get(index: number): T {
        return this._items[index]
    }

    /**
     * Replace the element at this index by another one.
     *
     * @listener Will trigger all registered updateListeners
     *
     * @param index The index. Should be > this.count
     * @param item The new item.
     */
    public set(index: number, item: T): void {
        let oldItem = this._items[index];
        this._items[index] = item;
        this._updateListener.forEach((callback) => callback(oldItem, item))
    }

    /**
     * Add a new item into the list.
     *
     * @listener Will trigger all registered addListeners
     *
     * @param item The item to add
     */
    public add(item: T): void {
        this._items.push(item);
        this._addListener.forEach((callback) => callback(item))
    }

    /**
     * Remove an item of this list.
     *
     * @listener Will trigger all registered removeListeners
     *
     * @param item The item to remove
     */
    public remove(item: T): void {
        let index = this._items.indexOf(item);
        this._items.splice(index, 1);
        this._removeListener.forEach((callback) => callback(item))
    }

    public get count(): number {
        return this._items.length
    }

    public get toArray(): T[] {
        let result: T[] = [];

        this._items.forEach((e) => result.push(e));

        return result
    }

    public forEach(f: (e: T) => void) {
        for (let i = 0; i < this._items.length; i++) {
            f(this._items[i])
        }
    }

    public contains(predicate: (e: T) => boolean): boolean {
        let found = false;

        for (let i = 0; i < this._items.length; i++) {
            if (predicate(this._items[i])) {
                found = true;
                break
            }
        }

        return found
    }

    public filter(f: (e: T) => boolean): List<T> {
        let result = new List<T>();

        for (let i = 0; i < this._items.length; i++) {
            if (f(this._items[i])) {
                result._items.push(this._items[i])
            }
        }

        return result
    }

    public map<E>(f: (e: T) => E): List<E> {
        let result = new List<E>();

        for (let i = 0; i < this._items.length; i++) {
            let erg = f(this._items[i]);
            result._items.push(erg)
        }

        return result
    }

    public reduce<E>(f: (e1: E, e2: T) => E, init: E): E {
        for (let i = 0; i < this._items.length; i++) {
            init = f(init, this._items[i])
        }

        return init
    }

    public registerAddListener(callback: (item: T) => void) {
        this._addListener.push(callback)
    }

    public registerRemoveListener(callback: (newItem: T) => void) {
        this._removeListener.push(callback)
    }

    public registerUpdateListener(callback: (oldItem: T, newItem: T) => void) {
        this._updateListener.push(callback)
    }
}
