/**
 * A parser which can transform a type into onother and backwards.
 *
 * @author Andrej Sajenko
 */
export interface Parser<F, T> {

    /**
     * @param f The data to transform.
     */
    from(f: F): T

    /**
     *
     * @param t The data to transform.
     */
    to(t: T): F
}
