/**
 * An observable object. Accepts callbacks which will be called if an event happen.
 *
 * @author Andrej Sajenko
 */
export interface Observable<T> {

    /**
     * @param callback The callback function which will be called if en element was added.
     */
    registerAddListener(callback: (item: T) => void): void

    /**
     * @param callback The callback function which will be called if an element was removed.
     */
    registerRemoveListener(callback: (oldItem: T) => void): void

    /**
     * @param callback The callback function which will be called if an element was replaced.
     */
    registerUpdateListener(callback: (oldItem: T, newItem: T) => void): void
}
