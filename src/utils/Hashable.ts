/**
 * This class can generate a hashCode.
 *
 * @author Andrej Sajenko
 */
export interface Hashable {

    /**
     * The generated hashCode
     *
     * @readOnly
     */
    hashCode: number
}