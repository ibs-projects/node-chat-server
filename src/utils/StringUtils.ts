/**
 * Utils for string manipulation.
 *
 * @author Andrej Sajenko
 */
export class StringUtils {

    /**
     * Check for a pattern match. Does a string start with an another one.
     *
     * @param str String which could contains another one at the start
     * @param searchString String to search for
     * @param position Where is the start position
     * @returns {boolean}
     */
    public static startsWith(str: string, searchString: string, position: number = 0) {
        return str.indexOf(searchString, position) === position;
    }
}
