/**
 *  The object can compare oneself to another.
 *
 * @author Andrej Sajenko
 */
export interface Comparable<T> {

    /**
     * Compares this object with the specified object for order.
     *
     * @param other The object to compare to.
     *
     * @return Return a number: negative = less, zero = equals or positive = greather then.
     */
    compareTo(other: T): number

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param other The other object.
     *
     * @return true if this item is equal to an another one.
     */
    equals(other: T): boolean
}
