import {List} from "../utils/List";
import {User} from "../models/User";

import {Map} from "../utils/Map";
import {ChatProtocolParser} from "../models/messages/ClientMessageParser";
import {Message} from "../models/Message";
import {ChatIO, Connection} from "./ChatIO";
import {
    LoginMessage, ErrorMessage, OkMessage, ParticipantsMessage,
    LogoutMessage, TextMessage, LeftMessage, JoinMessage, DistTextMessage, ClientMessage, ProtocolTypes
} from "../models/messages/chat-protocol";
import {ErrorUtils} from "../utils/ErrorUtils";

/**
 * The ChatController manage the flow of the chat protocol and controls the model and views.
 *
 * @author Andrej Sajenko
 */
export class ChatController {
    private connections = new Map<Connection, User>();
    private messages = new List<Message>();
    private parser = new ChatProtocolParser();

    /**
     * @param chatIO The network connection.
     */
    public constructor(private chatIO: ChatIO) {}

    /**
     * Init the model, view and connect them together and start the network connection to the chat server.
     */
    public start() {
        this.connections.onRemove((key: Connection, value: User) => {
            console.log(` --- User '${value.name}' left the chat.`);
            this.chatIO.distribute(this.parser.to(new LeftMessage(value.name)))
        });

        this.connections.onAdd((key: Connection, value: User) => {
            this.chatIO.distribute(this.parser.to(new JoinMessage(value.name)))
        });

        this.messages.registerAddListener((m: Message) => {
            this.chatIO.distribute(this.parser.to(new DistTextMessage(m.from.name, m.text, m.time)))
        });

        this.chatIO.onMessage((data: string, client: Connection) => {
            try {
                console.log( `----- received data -----\n${data}\n-------------------------` );
                let message: ClientMessage = this.parser.from(data);

                switch (message.type) {
                    case ProtocolTypes.LOGIN:
                        this.doLogin(<LoginMessage>message, client);
                        break;
                    case ProtocolTypes.LOGOUT:
                        this.doLogout(<LogoutMessage>message, client);
                        break;
                    case ProtocolTypes.TEXT_MESSAGE:
                        this.doTextMessage(<TextMessage>message, client);
                        break;
                    default:
                        client.send(this.parser.to(new ErrorMessage(`The message '${message.type}' is not ment to be send by the client.`)))
                }
            } catch (ex) {
                ErrorUtils.dump(ex);
                client.send(this.parser.to(new ErrorMessage("I Could not understand the following message: " + data)))
            }
        });

        this.chatIO.onClose((client: Connection) => {
            if (this.connections.keySet.some((k) => k.equals(client))) {
                this.connections.remove(client)
            }
        });

        this.chatIO.start()
    }

    private doLogin(loginMessage: LoginMessage, client: Connection) {
        let user = new User(loginMessage.username);

        if (this.connections.keySet.some((c) => c.equals(client))) {
            client.send(this.parser.to(new ErrorMessage("You already logged in throw this connection.")))
        } else if (this.connections.values.some((u) => u.equals(user))) {
            client.send(this.parser.to(new ErrorMessage(`The username '${user.name}' is already in use, pick another one`)))
        } else {
            // This user can connect with this username, reply with an ok message
            client.send(this.parser.to(new OkMessage()));
            
            this.connections.put(client, user);

            client.send(this.parser.to(new ParticipantsMessage(this.connections.values.map((u) => u.name))))
        }
    }

    private doLogout(logoutMessage: LogoutMessage, client: Connection) {
        if (this.connections.keySet.some(client.equals)) {
            this.connections.remove(client);
        }

        client.send(this.parser.to(new OkMessage()));

        client.close()
    }

    private doTextMessage(textMessage: TextMessage, client: Connection) {
        let user: User = this.connections.get(client);

        if (user) {
            this.messages.add(new Message(user, textMessage.text, Date.now()))
        } else {
            client.send(this.parser.to(new ErrorMessage("You are not connected - connect first")))
        }
    }
}
