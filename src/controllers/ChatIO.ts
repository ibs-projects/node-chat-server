import * as ws from "ws";
import * as http from "http";
import {Comparable} from "../utils/Comparable";

export class Connection implements Comparable<Connection> {
    public constructor(private _ws) {
    }

    public send(data: string) {
        this._ws.send(data)
    }

    public close() {
        this._ws.close()
    }

    public compareTo(other: Connection): number {
        return this.equals(other) ? 0 : -1;
    }

    public equals(other: Connection): boolean {
        return this === other;
    }
}

export class ChatIO {
    private wss: ws.Server;
    private onCloseListeners: ((client: Connection) => void)[] = [];
    private onMessageListeners: ((data: string, client?: Connection) => void)[] = [];

    public constructor(private server: http.Server) {
    }

    public start() {
        this.wss = new ws.Server({server: this.server});

        this.wss.on("connection", (socket) => {
            let client = new Connection(socket);

            socket.on("message", (message) => {
                this.onMessageListeners.forEach((cb) => cb(message, client))
            });

            socket.on("close", () => {
                this.onCloseListeners.forEach((cb) => cb(client))
            });
        })
    }

    public onMessage(receiveCallback: (data: string, client?: Connection) => void) {
        this.onMessageListeners.push(receiveCallback)
    }

    public onClose(closeCallback: (client: Connection) => void) {
        this.onCloseListeners.push(closeCallback)
    }

    public distribute(m: string) {
        this.wss.clients.forEach((client) => {
            client.send(m)
        })
    }

    public shutdown() {
        this.wss.close()
    }
}