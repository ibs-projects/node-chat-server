IBS Chat Server
===============
This is a chat server for students to practice cient side protocol implementation and websockets.

The [chat protocol](protocol.md) is a very simple protocol which is based an a connectionoriented communication protocol.


Install
-------
Glone this project with 

````
git clone https://git.thm.de/ibs-projects/node-chat-server.git
````

Install npm  with nodejs for your system 

[https://nodejs.org/en/]

Install all npm dependencies with

````
npm install
````

Build the project with

````
npm run build
````

Run / Start
-------

Use the config.json to configure the port to use

````json
{
  "port": 8081
}
````

Start the chat server with

````
npm start
````

License
-------
This project is licensed under the MIT License.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.